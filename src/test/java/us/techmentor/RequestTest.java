package us.techmentor;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RequestTest {
    @Test
    public void executeOpenAIQueryTest(){
        var request = new Request();

        // prod code:
        // 1) get functions
        // 2) put them in query
        // 3) put user query in
        // 4) send to open ai
        // 5) return result

        // test code:
        // 1) send nl query
        // 2) get result

        var result = request.executeOpenAIQuery("would you like to play a game?",null);
        assertEquals("stop",result.finishReason());
    }

    @Test
    public void executeOpenAIQueryTest_WithFunctionResults(){

    }
}
