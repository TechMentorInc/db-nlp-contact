package us.techmentor;

import org.junit.Test;

import static junit.framework.Assert.*;
import static org.mockito.Mockito.*;

public class NLPContactTest
{
    @Test
    public void testNLP()
    {
        var nlpContact = new NLPContact();
//        String result = nlpContact.processCommand("find me someone that works for D&B");
//        assertNotNull(result);
    }

    @Test
    public void testNLP_ProcessCommand(){
        var requestMock = mock(Request.class);
        var functionsMock = mock(Functions.class);
        when(functionsMock.execute("Test",null)).thenReturn("blah");
        var openAIResponse =
            new OpenAIResponse("function_call",null,
            new OpenAIResponse.Function("Test",null));
        var openAIResponse2 =
            new OpenAIResponse("stop","Yes",
            new OpenAIResponse.Function("Test",null));
        when(requestMock.executeOpenAIQuery("Yo",null))
            .thenReturn(openAIResponse);
        when(requestMock.executeOpenAIQuery("Yo","blah"))
            .thenReturn(openAIResponse2);

        var nlpContact = new NLPContact();
        nlpContact.request = requestMock;
        nlpContact.functions = functionsMock;
        var result = nlpContact.processCommand("Yo");

        verify(functionsMock).execute("Test",null);

        assertEquals("Yes",result);
    }
}
