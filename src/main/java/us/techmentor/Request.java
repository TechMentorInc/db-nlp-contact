package us.techmentor;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class Request {
    public OpenAIResponse executeOpenAIQuery(String prompt, String functionResults){
        return null;
    }

    private static final String DNB_AUTH_ENDPOINT = "https://api.dnb.com/auth/token";
    private static final String DNB_SEARCH_ENDPOINT = "https://api.dnb.com/Vx.x/organizations"; // Use the appropriate version and endpoint
    private static final String CLIENT_ID = "YOUR_CLIENT_ID";
    private static final String CLIENT_SECRET = "YOUR_CLIENT_SECRET";

    public static void main(String[] args) {
        String token = getDnbAccessToken();
        if (token != null) {
            searchBusinessContact(token, "Sample Business Name");
        }
    }

    private static String getDnbAccessToken() {
        try {
            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(DNB_AUTH_ENDPOINT);

            post.setHeader("Content-Type", "application/x-www-form-urlencoded");
            String body = "grant_type=client_credentials&client_id=" + CLIENT_ID + "&client_secret=" + CLIENT_SECRET;
            post.setEntity(new StringEntity(body));

            HttpResponse response = client.execute(post);
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                String result = EntityUtils.toString(entity);
                return result.split("\"access_token\":\"")[1].split("\"")[0];  // Quick way to extract the token, you might want to use a JSON parser instead
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static void searchBusinessContact(String token, String businessName) {
        try {
            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(DNB_SEARCH_ENDPOINT + "?keyword=" + businessName);  // Construct the URL with the business name or other criteria

            post.setHeader("Authorization", "Bearer " + token);

            HttpResponse response = client.execute(post);
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                String result = EntityUtils.toString(entity);
                System.out.println(result);  // This will print the raw JSON response. You might want to parse and process it further.
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
