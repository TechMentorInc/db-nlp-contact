package us.techmentor;

import java.util.Map;

public record OpenAIResponse(String finishReason,String assistant, Function function) {
    public record Function(String name, Map<String,String> arguments){};
}
