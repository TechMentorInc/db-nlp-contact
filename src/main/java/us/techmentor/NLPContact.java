package us.techmentor;

/**
 * Hello world!
 *
 */
public class NLPContact
{
    Request request = new Request();
    Functions functions = new Functions();

    public static void main( String[] args )
    {
        var nlpContact = new NLPContact();
        var done = false;
        var console = System.console();
        System.out.println("Would you like to play a game?\n");

        while(!done){
            System.out.print("] ");
            String command = console.readLine();
            if(command.equals("exit")) done=true;
            else System.out.println("\n"+nlpContact.processCommand(command));
        }
    }
    public String processCommand(String command){
        return executeCommand(command, null);
    }
    private String executeCommand(String command,String inFunctionResults){
        var result = request.executeOpenAIQuery(command,inFunctionResults);
        var function = result.function();
        if (result.finishReason().equals("function_call")) {
            var functionResults = functions.execute(function.name(), function.arguments());
            return executeCommand(command, functionResults);
        }
        else if (result.finishReason().equals("stop"))
            return result.assistant();
        else return null;
    }
}
